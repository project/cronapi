<?php

/**
 * Return the copy for specified text areas.
 * Just an internal function to keep things clean.
 */
function theme_cronapi_text_output($label, $var = '') {
  switch ($label) {
    case 'help_text':
      $output = t('help text goes here.');
    break;
    case 'form_admin_title':
      $output = t('<p>Provide a title for this preset.<br />');
      $output .= t('This title will only be used for user reference and <em>can</em> contain spaces.<br />');
      $output .= t('<em>The internal name for this preset is <strong>' . $var . '</strong> and can not be changed.</em><br />');
      $output .= t('<em>Modules may hook into this preset by calling <strong>hook_' . $var . '($op, $preset);</strong>.</em></p>');
    break;
    case 'form_admin_interval':
      $output = t('Provide a rotation interval for this preset.<br />');
      $output .= t('Interval settings dictate how often a preset should update.<br />');
      $output .= t('<strong>Allowed Values:</strong>This field is stored using PHP\'s strtotime() function so any Number [TimeToken] values are accepted. Only one Number [TimeToken] entry per preset.<br />');
      $output .= t('<strong><em>Examples:</em></strong> <em>4 hours</em> to updated this preset every 4 hours. <em>30 mins</em> or <em>30 minutes</em> would update this preset every 30 minutes. <em>7 days</em> would update every 7 days... etc..etc. If you want to update every day (aka 24 hours) you can specify <em>24 hours</em> to run every 24 hours or <em>1 day</em> to run every day.<br />');
      $output .= t('<em>module accepts: mins, min, hours, hour, days, day, weeks, week, years, year<br /></em>');
      $output .= t('<strong>NOTE: All presets are confined by the restrictions of the current crontab settings. If you have cron set to run once every hour and a preset set to run every <em>30 mins</em>, the preset will only update every hour since this is when cron is set to be called.</strong>');
    break;
    case 'form_admin_time':
      $output = t('Optionally specify a start time for this preset to update.<br />');
      $output .= t('Presets are ran against there next rotation iteration timestamp. If you want to build a preset and want it to begin rotating at a specific point in the future, then enter that date here.<br />');
      $output .= t('<strong>Time Format: </strong>Valid strtotime() strings are accepted. <em>January 1 2008 12:00 pm</em> is just as valid as <em>2008-01-01 12:00</em><br />');
      $output .= t('<strong>This field will overwrite the specified Interval Rotation until the date specified is reached.</strong>');
    break;
    case 'form_admin_enable':
      $output = t('This is a manual override for preset rotation. If a preset is active and then is turned off. Then that preset will stay the same until it is turned on again. If a preset is off, then no cron actions will occur.');
    break;
    case 'admin_preset_list':
      $output = t('<p>Preset information is updated on each page load for administration.<br />');
      $output .= t('<em>Next Scheduled Updates</em> do not always match the expected <em>Schedule</em> setting. Crontab and user settings effect the consistency of the preset display information.</p>');
    break;
    default:
      $output = t('default');
    break;
  }
  
  return $output;
}// end - function


/**
 * theme function to display the preset list
 */
function theme_cronapi_admin_list($presets) {
  $return = theme('cronapi_text_output', 'admin_preset_list');
  
  $total  = count($presets);
  
  if ($total >= 1) {
    $rows   = array();
    $header = array(t('ID'), t('Title'), t('Name'), t('Module'), t('Schedule'), t('Last Update'), t('Next Scheduled Update'), t('Status'), array('data' => t('Operations'), 'colspan' => '3'));
    foreach ($presets as $module_title => $preset) {
      $rows[] = array(
        'pid'            => $preset['pid'],
        'title'          => $preset['title'],
        'name'           => $preset['name'],
        'module'         => $preset['module'],
        'schedule'       => 'Every ' . $preset['rotation_int'],
        'last_update'    => date('Y-m-d h:i:s a', $preset['current_update']),
        'next_update'    => date('Y-m-d h:i:s a', $preset['next_update']),
        'active'         => ($preset['enabled'])? t('ON'): t('OFF'),
        'edit'           => l(t('edit'), 'admin/settings/cronapi/edit/preset/' . $preset['pid']),
        'run'            => l(t('invoke'), 'admin/settings/cronapi/invoke/preset/' . $preset['pid']),
      );
    }// end - foreach
    
    $return .= theme('table', $header, $rows, array('class' => 'cronapi_configured_list'));
  } else {
    $rows[] = array(array('data' => t('No presets available.'), 'colspan' => '9'));
    $header = array(t('ID'), t('Title'), t('Name'), t('Module'), t('Schedule'), t('Last Update'), t('Next Scheduled Update'), t('Status'), array('data' => t('Operations'), 'colspan' => '3'));
    $return .= theme('table', $header, $rows, array('class' => 'cronapi_configured_list'));
  }
  
  return $return;
}//end - function


/**
 * Theme the unconfigured preset list
 */
function theme_cronapi_unconfigured_prest_list($presets) {
  $return = theme('cronapi_text_output', 'admin_preset_list');
  
  $total = count($presets);
  
  if ($total >= 1) {
    $rows   = array();
    $header = array(t('Title'), t('Name'), t('Module'), t('Operations'));
    foreach ($presets as $module_title => $preset) {
      $rows[] = array(
        'title'          => $preset['title'],
        'name'           => $preset['name'],
        'module'         => $preset['module'],
        'edit'           => l(t('configure'), 'admin/settings/cronapi/edit/preset', array(), 'title=' . $preset['title'] . '&module=' . $preset['module'] . '&name=' . $preset['name']),
      );
    }//end - foreach
    
    $return .= theme('table', $header, $rows, array('class' => 'cronapi_unconfigured_list'));
  } else {
    $rows[] = array(array('data' => t('No presets available.'), 'colspan' => '4'));
    $header = array(t('Title'), t('Name'), t('Module'), t('Operations'));
    $return .= theme('table', $header, $rows, array('class' => 'cronapi_unconfigured_list'));
  }
  
  return $return;
}// end - function
