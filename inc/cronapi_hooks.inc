<?php

/**
 * Implementation of hook_help
 */
function cronapi_help($section) {
  switch ($section) {
    case 'admin/help#cronapi':
      $output = theme('cronapi_text_output', 'help_text');
      return $output;
  }
}//end - function

/**
 * Implementation of hook_perm
 */
function cronapi_perm() {
  return array('configure cronapi presets');
}//end - function

/**
 * Implementation of hook_menu
 */
function cronapi_menu($may_cache) {
  $items = array();
  
  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/cronapi',
      'title' => t('Cronapi Presets'),
      'description' => t('Manage scheduling presets for your site.'),
      'callback' => 'cronapi_preset_administration_list',
      'access' => user_access('configure cronapi presets')
    );
    $items[] = array(
      'path' => 'admin/settings/cronapi/list',
      'title' => t('Configured Presets'),
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
    );
    $items[] = array('path' => 'admin/settings/cronapi/edit/preset',
      'title' => t('Configure Preset'),
      'callback' => 'cronapi_admin_preset_edit',
      'access' => user_access('configure cronapi presets'),
      'type' => MENU_CALLBACK,
    );
    $items[] = array('path' => 'admin/settings/cronapi/unconfigured/list',
      'title' => t('Unconfigured Presets'),
      'callback' => 'cronapi_admin_preset_new_list',
      'access' => user_access('configure cronapi presets'),
      'type' => MENU_LOCAL_TASK,
    );
    $items[] = array('path' => 'admin/settings/cronapi/invoke/preset',
      'title' => t('Invoke Preset'),
      'callback' => 'cronapi_admin_preset_invoke',
      'access' => user_access('configure cronapi presets'),
      'type' => MENU_CALLBACK,
    );
  }
  
  return $items;
}//end - function


/**
 * Form return function for _menu callback
 * for the Cronapi Edit Preset
 * $param $edit
 *   Array containing the form elements if the form is being updated.
 * return $form
 */
function cronapi_preset_form($edit = array()) {
  $form['cronapi_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cronapi Preset'),
  );
  
  $form['cronapi_settings']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $edit['title'],
    '#size' => 60,
    '#maxlength' => 64,
    '#description' => theme('cronapi_text_output', 'form_admin_title', $edit['name']),
    '#required' => TRUE,
  );
  
  $form['cronapi_settings']['name'] = array(
    '#type'          => 'hidden',
    '#value' => $edit['name'],
  );
  
  $form['cronapi_settings']['module'] = array(
    '#type'          => 'hidden',
    '#value' => $edit['module'],
  );
  
  $form['cronapi_settings']['rotation_int'] = array(
    '#type' => 'textfield',
    '#title' => t('Interval Rotation'),
    '#default_value' => $edit['rotation_int'],
    '#size' => 60,
    '#maxlength' => 64,
    '#description' => theme('cronapi_text_output', 'form_admin_interval'),
    '#required' => TRUE,
  );
  
  $default_start_time = date('Y-m-d h:i:s a', $edit['next_update']);
  $default_user_time_display = ($edit['next_update'] < time())? t('') : $default_start_time;
  $form['cronapi_settings']['next_update'] = array(
    '#type' => 'textfield',
    '#title' => t('User Defined Start Date/Time'),
    '#default_value' => $default_user_time_display,
    '#size' => 60,
    '#maxlength' => 64,
    '#description' => theme('cronapi_text_output', 'form_admin_time'),
  );
  
  $form['cronapi_settings']['enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Preset'),
    '#default_value' => $edit['enabled'],
    '#description' => theme('cronapi_text_output', 'form_admin_enable'),
  );
  
  $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
  
  if ($edit['pid']) {
    $form['submit'] = array('#type' => 'submit', '#value' => t('Update'));
    $form['pid'] = array('#type' => 'value', '#value' => $edit['pid']);
    $form['delete'] = array('#type' => 'submit', '#value' => t('Reset'));
  }
  
  return $form;
}//end - function


/**
 * Preset Administration Form Validation
 */
function cronapi_preset_form_validate($form_id, $form_values) {
  $name = (preg_match("/^[a-z][\w]+$/", $form_values['name']))? TRUE : FALSE;
  $module = (preg_match("/^[a-z][\w]+$/", $form_values['module']))? TRUE : FALSE;
  $rotation = (strtotime($form_values['rotation_int']))? TRUE : FALSE;
  $update_date = (!empty($form_values['next_update']))? strtotime($form_values['next_update']): TRUE;
  if (!$name) { form_set_error($form_id, "This preset name is not valid."); }
  if (!$module) { form_set_error($form_id, "The module that is trying to install this preset does not have a valid name."); }
  if (!$rotation) { form_set_error($form_id, "Please enter a valid rotation interval."); }
  if (!$update_date) { form_set_error($form_id, "The Start Date you entered is not a valid format. Please enter a valid date/time string."); }
}//end - function


/**
 * Preset Administration Form Submit
 * Populates an array from the form elements
 * and passes the data to the DB INSERT function.
 */
function cronapi_preset_form_submit($form_id, $form_values) {
  $form_update_time = strtotime($form_values['next_update']);
  if ($form_update_time && $form_update_time > time()) {
    $next_rotation = $form_update_time;
  } else {
    $new_times = cronapi_rotate_preset_time($form_values);
    $next_rotation = $new_times['update_time'];
  }
  
  $variables = array(
    'title'           => t($form_values['title']),
    'name'            => t($form_values['name']),
    'module'          => t($form_values['module']),
    'rotation_int'    => t($form_values['rotation_int']),
    'current_update'  => time(),
    'next_update'     => $next_rotation,
    'instantiated'    => 0,
    'enabled'         => $form_values['enable'],
    'created'         => time(),
    'updated'         => time(),
  );
  
  switch (cronapi_save_preset($form_values)) {
    case 'SAVED_NEW_PRESET':
      cronapi_invoke_cronapi('insert', $variables);
      cronapi_db_insert_new_preset($variables);
    break;
    case 'SAVED_UPDATED_PRESET':
      $variables['pid'] = $form_values['pid'];
      cronapi_invoke_cronapi('update', $variables);
      cronapi_db_update_preset($variables);
    break;
    case 'DELETED_PRESET':
    break;
  }
  
  return 'admin/settings/cronapi';
}//end - function


/**
 * Confirm Deletion of a preset
 */
function cronapi_preset_confirm_delete($pid) {
  $preset = cronapi_get_preset($pid);

  $form['type'] = array('#type' => 'value', '#value' => 'preset');
  $form['pid'] = array('#type' => 'value', '#value' => $pid);
  $form['title'] = array('#type' => 'value', '#value' => $preset['title']);
  return confirm_form($form, t("Are you sure you want to reset the preset %title?", array('%title' => $preset['title'])), 'admin/settings/cronapi', t('Deleting a preset will cause the preset to stop being called on _cron.'), t('Reset'), t('Cancel'));
}//end - function


/**
 * Confirm Delete preset submit function
 */
function cronapi_preset_confirm_delete_submit($form_id, $form_values) {
  $preset = cronapi_get_preset($form_values['pid']);
  cronapi_invoke_cronapi('delete', $preset);
  
  cronapi_db_delete_preset($form_values['pid']);
  drupal_set_message(t('Cronapi preset %name has been reset to unconfigured.', array('%name' => $form_values['title'])));
  return 'admin/settings/cronapi';
}//end - function

/**************************************
 ** CRON Function **
 *************************************/
/**
 * hook_cron implementation
 * runs the rotation scripts for each preset that should be updated.
 */
function cronapi_cron() {
  $time = time();
  $presets = cronapi_get_presets(array('enabled' => 1, 'next_update' => '<' . $time, ));
  
  foreach ($presets as $idx => $preset) {
    cronapi_rotate_preset($preset);
  }
}//end - function