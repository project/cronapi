<?php


/**
 * Load Presets function
 * @param $criteria
 *   An array that specifies the criteria to query against.
 *   eg: array('pid' => 12) or array('rotation_int' => '1 hour', 'next_update' => '<12345609809)
 * @param $count_ony
 *   Boolean to indicate if the query should only return the number of rows.
 * return
 *   A resource ID.
 */
function cronapi_get_presets($criteria = array(), $fields = '', $count_only=FALSE) {
  $return_array = TRUE;
  if ($criteria == '*') {
    $query = "SELECT * FROM {cronapi_presets}";
    
    $return = array();
    $result = db_query($query);
    while ($row = db_fetch_array($result)) {
      $return[$row['module'] . '_' . $row['name']] = $row;
    }// end - while
  
  return $return;
  }
  if ($count_only) {
    $query = "SELECT COUNT(pid) FROM {cronapi_presets} WHERE ";
  } 
  elseif (!empty($fields)) {
    $query = "SELECT $fields FROM {cronapi_presets} WHERE ";
  }
  else {
    $query = "SELECT * FROM {cronapi_presets} WHERE ";
  }
  
  $args  = array();
  
  foreach($criteria as $column => $value) {
    $column = strtolower($column);
    $query .= (count($args) < 1)? '' : 'AND ';
    switch ($column) {
      case 'pid':
        $return_array = FALSE;
      case 'instantiated':
      case 'enabled':
        $query .= "$column = %d ";
        $args[]  = (int)$value;
      break;
      case 'title':
      case 'name':
      case 'module':
      case 'rotation_int':
        $query .= "$column = '%s' ";
        $args[]  = (string)$value;
      break;
      case 'current_update':
      case 'next_update':
      case 'created':
      case 'updated':
        // for the dates, the first character of $value should be the operator, i.e. '<', '=' or '>'
        // the remainder should be the value: either a (numeric) Unix timestamp, or a string formatted
        //   to match MySQL's date/timestamp format
        //   e.g. 'created' => '>2006-12-31 23:59:59' to find presets created in 2007 or later
        $operator = substr($value, 0, 1);
        $value    = substr($value, 1);
        if (is_numeric($value)) {
          //$value = date('Y-m-d H:i:s', (int)$value);
        }
        $query  .= "{$column}{$operator}'%s' ";
        $args[]  = $value;
      break;
      default:
        // invalid criteria
        return NULL;
      break;
    }//end - switch ($column)
  }//end - foreach
  
  // if all we want is a count, get it and return it
  if ($count_only) {
    return db_result(db_query($query, $args));
  }
  
  $return_all = array();
  $result = db_query($query, $args);
  while ($row = db_fetch_array($result)) {
    $return_all[$row['module'] . '_' . $row['name']] = $row;
    $return_one = $row;
  }// end - while
  
  $return = ($return_array)? $return_all: $return_one;
  
  return $return;
}//end - function


/**
 * DB Insert of new preset
 * @param $vars
 *   An array containing all the data to be stored on initial db storage.
 */
function cronapi_db_insert_new_preset($vars) {
  db_query("INSERT INTO {cronapi_presets} (title, name, module, rotation_int, current_update, next_update, instantiated, enabled, created, updated) 
  VALUES ('%s', '%s', '%s', '%s', %d, %d, %d, %d, %d, %d)", $vars['title'], $vars['name'], $vars['module'], $vars['rotation_int'], $vars['current_update'], $vars['next_update'], $vars['instantiated'], $vars['enabled'], $vars['created'], $vars['updated']);
  drupal_set_message(t('The Cronapi preset ' . $vars['title'] . ' has been saved.'));
}//end - function


/**
 * DB preset update
 * @param $vars
 *   An array containing the complete preset array data.
 */
function cronapi_db_update_preset($vars) {
  db_query("UPDATE {cronapi_presets} SET title = '%s', name = '%s', module = '%s', rotation_int = '%s', current_update = %d, next_update = %d, instantiated = %d, enabled = %d, updated = %d WHERE pid = %d", 
  $vars['title'], $vars['name'], $vars['module'], $vars['rotation_int'], $vars['current_update'], $vars['next_update'], $vars['instantiated'], $vars['enabled'], $vars['updated'], $vars['pid']);
  drupal_set_message(t('The Cronapi preset ' . $vars['title'] . ' has been updated.'));
}//end - function


/**
 * Delete a preset.
 *
 * @param $pid
 *   A preset ID.
 * @return
 *   String indicating items were deleted.
 */
function cronapi_db_delete_preset($pid) {
  db_query('DELETE FROM {cronapi_presets} WHERE pid = %d', $pid);
  cache_clear_all();
}//end - function